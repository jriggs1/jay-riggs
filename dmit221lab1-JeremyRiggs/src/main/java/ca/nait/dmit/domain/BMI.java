package ca.nait.dmit.domain;

import javax.validation.constraints.*;

/**
 * This class is use to calculate a person's body mass index (BMI) and their BMI
 * Category.
 * 
 * @author Jay Riggs
 * @version 2012.10.18
 */
public class BMI {

	/**
	 * This is the variable for the persons weight
	 * 
	 * @return
	 */
	@NotNull(message = "Weight cannot be null")
	@Min(value=40, message="Weight must be at least 40 lbs")
	private int weight;

	/**
	 * This is the variable for the persons height in feet
	 * 
	 * @return
	 */
	@NotNull(message = "Height in feet cannot be null")
	@Min(value=3, message="Height must be at least 3 feet")
	private int heightFeet;

	/**
	 * This is the variable for the persons height in inches
	 * 
	 * @return
	 */
	@NotNull(message = "Height in inches cannot be null")
	@DecimalMax(value="11.99", message="Height must be less than 12 inches")
	private double heightInches;

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeightFeet() {
		return heightFeet;
	}

	public void setHeightFeet(int heightFeet) {
		this.heightFeet = heightFeet;
	}

	public double getHeightInches() {
		return heightInches;
	}

	public void setHeightInches(double heightInches) {
		this.heightInches = heightInches;
	}

	public BMI() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BMI(int weight, int heightFeet, double heightInches) {
		super();
		this.weight = weight;
		this.heightFeet = heightFeet;
		this.heightInches = heightInches;
	}

	/**
	 * This method converts the height in feet to inches, then adds it to the
	 * current inches. This makes it easier to calculate the BMI because you
	 * just use inches instead of feet and inches.
	 * 
	 * @return heightInInches
	 */
	public double getHeightInInches() {
		double heightInInches;
		heightInInches = (heightFeet * 12) + heightInches;
		return heightInInches;
	}

	/**
	 * Calculate the body mass index (BMI) using the weight and height of the
	 * person. The BMI of a person is calculated using the formula: BMI = 700 *
	 * weight / (height * height) where weight is in pounds and height is in
	 * inches.
	 * 
	 * @return the body mass index (BMI) value of the person
	 */
	public double getBMI() {
		return 703 * weight / Math.pow(getHeightInInches(), 2);
	}

	/**
	 * Determines the BMI Category of the person using their BMI value and
	 * comparing it against the following table.
	 * ----------------------------------------- | BMI range | BMI Category |
	 * |---------------------------------------| | < 18.5 | underweight | | >=
	 * 18.5 and < 25 | normal | | >= 25 and < 30 | overweight | | >= 30 | obese
	 * | -----------------------------------------
	 * 
	 * @return one of following: underweight, normal, overweight, obese.
	 */
	public String getBMICategory() {
		String BMICategory = null;

		if (getBMI() < 18.5) {
			BMICategory = "Underweight";
		} else if (getBMI() >= 18.5 && getBMI() <= 24.9) {
			BMICategory = "Normal";
		} else if (getBMI() >= 25 && getBMI() <= 29.9) {
			BMICategory = "Overweight";
		} else if (getBMI() >= 30) {
			BMICategory = "Obese";
		}
		return BMICategory;
	}
}
