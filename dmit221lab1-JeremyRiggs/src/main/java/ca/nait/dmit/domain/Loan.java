package ca.nait.dmit.domain;

public class Loan {

	private double mortgageAmount;

	private double annualInterestRate;

	private int amortizationPeriod;

	public Loan() {
		super();
		// TODO Auto-generated constructor stub
	}

	public double getMortgageAmount() {
		return mortgageAmount;
	}

	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}

	public double getAnnualInterestRate() {
		return annualInterestRate;
	}

	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}

	public int getAmortizationPeriod() {
		return amortizationPeriod;
	}

	public void setAmortizationPeriod(int amortizationPeriod) {
		this.amortizationPeriod = amortizationPeriod;
	}

	public Loan(double mortgageAmount, double annualInterestRate,
			int amortizationPeriod) {
		setMortgageAmount(mortgageAmount);
		setAnnualInterestRate(annualInterestRate);
		setAmortizationPeriod(amortizationPeriod);
	}

	public double getMonthlyPayment() {
		double monthlyPayment;
		double topHalf;
		double bottomHalf;

		topHalf = Math.pow(1 + annualInterestRate / 200, 1.0 / 6.0);
		bottomHalf = Math.pow(topHalf, -12 * amortizationPeriod);
		monthlyPayment = roundTo2Decimals(mortgageAmount
				* ((topHalf - 1) / (1 - bottomHalf)));
		return monthlyPayment;
	}

	public LoanSchedule[] getLoanScheduleArray() {

		// number of payments is the loan term (in years) multiply by the number
		// of months per year (12)
		int numberOfPayments = amortizationPeriod * 12;
		double interestPaid = 0;
		double monthlyPercentageRate = Math.pow(1 + annualInterestRate / 200,
				1.0 / 6.0) - 1;
		double principalPaid = 0;
		double monthlyPayment = getMonthlyPayment();
		LoanSchedule[] loanScheduleArray = new LoanSchedule[numberOfPayments];
		// set the initial remaining balance is equal to amount borrowed
		double remainingBalance = mortgageAmount;
		// calculate monthlyPercentageRate
		for (int paymentNumber = 1; paymentNumber <= numberOfPayments; paymentNumber++) {
			// calculate interestPaid and round to 2 decimal places
			interestPaid = monthlyPercentageRate * remainingBalance;
			interestPaid = roundTo2Decimals(interestPaid);
			// calculate principalPaid and round to 2 decimal places
			principalPaid = monthlyPayment - interestPaid;
			principalPaid = roundTo2Decimals(principalPaid);
			// update remainingBalance and round to 2 decimal places
			if (remainingBalance < monthlyPayment) {

				principalPaid = remainingBalance;
			}
			remainingBalance = remainingBalance - principalPaid;
			remainingBalance = roundTo2Decimals(remainingBalance);

			// set remainingBalance to zero if it is calculated to be less than
			// zero
			if (remainingBalance < 0) {
				remainingBalance = 0;
			}
			// arrays in Java are 0-index based
			loanScheduleArray[paymentNumber - 1] = new LoanSchedule(
					paymentNumber, interestPaid, principalPaid,
					remainingBalance);
		}
		return loanScheduleArray;
	}

	public static double roundTo2Decimals(double valueToRound) {
		return Math.round(valueToRound * 100) / 100.0;
	}
}
