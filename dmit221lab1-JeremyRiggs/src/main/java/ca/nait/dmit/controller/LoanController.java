package ca.nait.dmit.controller;

import java.text.NumberFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;


import util.JSFUtil;
import ca.nait.dmit.domain.Loan;
import ca.nait.dmit.domain.LoanSchedule;

@ManagedBean(name = "loanBean")
@SessionScoped
public class LoanController {

	private Loan currentLoan = new Loan();
	private LoanSchedule[] loanScheduleTable;
	private CartesianChartModel mortgageChart;
	
	public void calculatePayment()
	{
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();	
		loanScheduleTable = currentLoan.getLoanScheduleArray();
		JSFUtil.addInfoMessage("Your monthly payment is " + numberFormat.format(currentLoan.getMonthlyPayment()));
		ChartSeries remainingBalance = new ChartSeries();
		remainingBalance.setLabel(currentLoan.getMortgageAmount() + " at " + currentLoan.getAnnualInterestRate() + " for " + currentLoan.getAmortizationPeriod());
		
		for(int count = 1; count <= currentLoan.getAmortizationPeriod(); count++)
		{
			remainingBalance.set(count, loanScheduleTable[count * 12 - 1].getRemainingBalance());
		}
		mortgageChart.addSeries(remainingBalance);
	}
	public Loan getCurrentLoan() {
		return currentLoan;
	}
	public void setCurrentLoan(Loan currentLoan) {
		this.currentLoan = currentLoan;
	}
	public LoanSchedule[] getLoanScheduleTable() {
		return loanScheduleTable;
	}
	public CartesianChartModel getMortgageChart() {
		return mortgageChart;
	}
	public void setMortgageChart(CartesianChartModel mortgageChart) {
		this.mortgageChart = mortgageChart;
	}
	public void setLoanScheduleTable(LoanSchedule[] loanScheduleTable) {
		this.loanScheduleTable = loanScheduleTable;
	}
	public LoanController() {
		super();
		// TODO Auto-generated constructor stub
		mortgageChart = new CartesianChartModel();
	}
}
