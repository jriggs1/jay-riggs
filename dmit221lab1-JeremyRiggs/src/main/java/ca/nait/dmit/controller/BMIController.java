package ca.nait.dmit.controller;

import java.text.NumberFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ca.nait.dmit.domain.BMI;

import util.JSFUtil;

@ManagedBean(name = "bmiBean")
@ViewScoped
public class BMIController {

	private int weight;
	private int heightFeet;
	private int heightInches;
	private double thisBMI;

	public double getThisBMI() {
		return thisBMI;
	}

	public void setThisBMI(double thisBMI) {
		this.thisBMI = thisBMI;
	}

	public BMI getNewBMI() {
		return newBMI;
	}

	public void setNewBMI(BMI newBMI) {
		this.newBMI = newBMI;
	}

	private BMI newBMI = new BMI();

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeightFeet() {
		return heightFeet;
	}

	public void setHeightFeet(int heightFeet) {
		this.heightFeet = heightFeet;
	}

	public int getHeightInches() {
		return heightInches;
	}

	public void setHeightInches(int heightInches) {
		this.heightInches = heightInches;
	}

	public BMIController() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void calculateBMI() {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		numberFormat.setMaximumFractionDigits(2);
		String bmiValue = numberFormat.format(newBMI.getBMI());

		if (newBMI.getBMICategory() == "Underweight") {
			JSFUtil.addInfoMessage("Your BMI is " + bmiValue
					+ ", you are underweight.");
		}
		if (newBMI.getBMICategory() == "Normal") {
			JSFUtil.addInfoMessage("Your BMI is " + bmiValue
					+ ", you are normal");
		} else if (newBMI.getBMICategory() == "Overweight") {
			JSFUtil.addWarningMessage("Warning!  Your BMI is " + bmiValue
					+ ", you are overweight");
		} else if (newBMI.getBMICategory() == "Obese") {
			JSFUtil.addErrorMessage("Warning!  Your BMI is "
					+ bmiValue
					+ ", you are at risk of developing heart disease, please see your family doctor.");
		}
	}
}
